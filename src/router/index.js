import Vue from "vue"
import VueRouter from "vue-router"
import Home from "../views/Home.vue"
import User from "../views/User.vue"

Vue.use(VueRouter)

const routes = [
	{
		path: "/",
		name: "Dashboard",
		component: Home
	},
	{
		path: "/users/:id",
		name: "User Detail",
		component: User
	}
]

const router = new VueRouter({
	mode: "history",
	base: process.env.BASE_URL,
	routes
})

export default router
