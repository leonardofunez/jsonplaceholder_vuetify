# JSON Placeholder API + Vue + Vuetify

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Additional installed dependencies
`Axios, Vuex and Vuetify`

### Deployed in Netlify
[Visit the website](https://jsonplaceholder-vuetify.netlify.app/)

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
